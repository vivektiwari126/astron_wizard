<?php include 'connection.php' ?>

<?php
  header('Content-Type: application/json; charset=utf-8');
//   $json_input_data=json_decode(file_get_contents('php://input'),TRUE);
    if($_SERVER["REQUEST_METHOD"] == "GET")
     {
      try {
       $sql ="SELECT * FROM astron_astrologer";
      if ($result = $conn -> query($sql)) {
          if($result->num_rows > 0)
          {
               while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
               {
               $rows[] = $row;
               }
               $Msg=["msg"=>"User's astrologer fetch successfully.","data"=>$rows ,"status"=>1];
              echo json_encode($Msg);
          }else{
              $Msg=["msg"=>"User's astrologer not fetch successfully.","status"=>0];
              echo json_encode($Msg);
          }
         $conn->close();
         }
      }catch(Exception $e)
        {
            exit;
        }
      }else{
          echo json_encode(["requestType:"=>"Get request not supported.Please use get request."]);
      }
    
?>
