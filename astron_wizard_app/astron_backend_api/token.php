
<?php
include 'connection.php';
require_once("PaytmChecksum.php");
require_once("keys.php");
$data = json_decode(file_get_contents('php://input'), true);
$orderId = $data["orderId"];
$amt = $data["amt"];
$emailId = $data["emailId"];
$mobileNo = $data["mobileNo"];
$paytmParams = array();
$paytmParams["body"] = array(
    "requestType"   => "Payment",
    "mid"           => $mid,
    "websiteName"   => "WEBSTAGING",
    "orderId"       => $orderId,
    "callbackUrl" => "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=".$orderId,
    
     # Enable it for testing environment.....
    
     #"callbackUrl" => "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=".$orderId,
    
    "txnAmount"     => array(
        "value"     => $amt,
        "currency"  => "INR",
    ),
    "userInfo"      => array(
        "custId"    => "CUST_"+$mobileNo,
    ),
);
$checksum = PaytmChecksum::generateSignature(json_encode($paytmParams["body"], JSON_UNESCAPED_SLASHES), $merchant_Key);

$paytmParams["head"] = array(
    "signature"    => $checksum
);

$post_data = json_encode($paytmParams, JSON_UNESCAPED_SLASHES);

/* for Staging */
#$url = "https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction?mid=".$mid."&orderId=".$orderId;

/* for Production */
 $url = "https://securegw.paytm.in/theia/api/v1/initiateTransaction?mid=".$mid."&orderId=".$orderId;

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json")); 
$response = curl_exec($ch);

	     $status=1;
      	 $t=time();
      	 $date_initial=date("Y-m-d",$t);
      	 $custId= "CUST_"+$mobileNo;
         $sql="INSERT INTO astron_initiatetransaction (emailId,jsonData,status,createdBy,modifiedBy,modifiedOn,createdOn,custId,amount,orderId) VALUES ('".$emailId."','$response',$status,$status,$status,'".$date_initial."','".$date_initial."','".$custId."',$amt,'".$orderId."')";
  
  if (!mysqli_query($conn, $sql)) {
    //echo "Error: " . mysqli_error($conn);
}       
echo $response;

 

?>