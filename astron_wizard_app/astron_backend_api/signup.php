<?php include 'connection.php'; ?>

<?php
  header('Content-Type: application/json; charset=utf-8');
  $json_input_data=json_decode(file_get_contents('php://input'),TRUE);
    if($_SERVER["REQUEST_METHOD"] == "POST")
     {
      try {
       $fullName = trim($json_input_data["fullName"]);
       $emailId = trim($json_input_data["emailId"]);
       $mobileNo = trim($json_input_data["mobileNo"]);
       $password = trim($json_input_data["password"]);
       $confirmPassword = trim($json_input_data["confirmPassword"]);
       if(empty($fullName)){
          $fullName=["fullName"=>"Please enter full name."];
          echo json_encode($fullName);
          return 1; 
       } 
       if(empty($emailId)){
          $emailId=["emailId"=>"Please enter emailid."];
          echo json_encode($emailId);
           return 1; 
       } 
      if(empty($mobileNo)){
          $mobileNo=["mobileNo"=>"Please enter mobileNo."];
          echo json_encode($mobileNo);
           return 1; 
       } 
       if(empty($password)){
          $password=["password"=>"Please enter password."];
          echo json_encode($password);
           return 1; 
       } 
       if(empty($confirmPassword)){
          $confirmPassword=["confirmPassword"=>"Please enter confirm password."];
          echo json_encode($confirmPassword);
           return 1; 
       } 
       if($password!=$confirmPassword){
          $password=["password"=>"Please enter password and confirm password are same.","msg"=>"Please enter password and confirm password are same."];
          echo json_encode($password);
           return 1; 
       } 
      
      	 $status=1;
      	 $t=time();
      	 $date_initial=date("Y-m-d",$t);
         $sql1="INSERT INTO astron_users (fullName,emailId,mobileNo,password,status,createdBy,modifiedBy,modifiedOn,createdOn) VALUES ('".$fullName."' , '".$emailId."','".$mobileNo."','".$password."','.$status.','.$status.','.$status.','".$date_initial."','".$date_initial."')";
		$sql2="Insert into astron_login (emailId,mobileNo,password,status) values('".$emailId."','".$mobileNo."','".$password."','.$status.')";
       
       	if ($conn->query($sql1) === TRUE && $conn->query($sql2) === TRUE)
				{
				 $Msg=["msg"=>"Account Created Successfully.","status"=>1];
                 echo json_encode($Msg);
			    
		}else{
		     $Msg=["msg"=>"Account not created successfully. Please contact astron wizard guru.","status"=>0];
             echo json_encode($Msg);
		}
       
         $conn->close();
         
       }catch(Exception $e)
        {
            exit;
        }
      }else{
          echo json_encode(["requestType:"=>"Get request not supported.Please use post request."]);
      }
    
?>