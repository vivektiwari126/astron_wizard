<?php include 'connection.php' ?>

<?php
  header('Content-Type: application/json; charset=utf-8');
  $json_input_data=json_decode(file_get_contents('php://input'),TRUE);
    if($_SERVER["REQUEST_METHOD"] == "POST")
     {
      try {
    
       $astrologerMobileNo = trim($json_input_data["astrologerMobileNo"]);
       $sql ="SELECT * FROM astron_astrologer WHERE astrologerMobileNo='$astrologerMobileNo' and isAvailable!='Busy'";
     
      if ($result = $conn -> query($sql)) {
          if($result->num_rows > 0)
          {
            $Msg=["msg"=>"Astrologer is available.","data"=> $result->fetch_assoc(),"status"=>1];
            echo json_encode($Msg);
          }else{
              $Msg=["msg"=>"Astrologer is not available. Please contact to another astrologer.","status"=>0];
              echo json_encode($Msg);
          }
         $conn->close();
         }
      }catch(Exception $e)
        {
            exit;
        }
      }else{
          echo json_encode(["requestType:"=>"Get request not supported.Please use post request."]);
      }
    
?>
