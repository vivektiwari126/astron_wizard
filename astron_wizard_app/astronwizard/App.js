/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import Routes from "./src/navigator/Routes";
import { UserProvider } from './src/services/logincontext/UserContext'
import { UserBalanceProvider } from "./src/services/logincontext/UserBalanceContext";
const App = () => {
  return (
    <UserProvider>
      <UserBalanceProvider>
        <Routes />
      </UserBalanceProvider>
    </UserProvider>
  );
};


export default App;
