import React, { createContext, useState, useEffect } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Constants } from "../../constant/Constants";


export const UserContext = createContext({
    setUserLogin: () => { },
    userLogin: null,
});

export const UserProvider = ({ children }) => {
    const [userLogin, setUserLogin] = useState(null);
    useEffect(() => {

        if (userLogin == null) {
            AsyncStorage.getItem('userLogin').then((res) => {
                if (res != null) {
                    // setUserLogin(res)
                    setUserLogin(JSON.parse(res))
                    // console.log('====================================');
                    // console.log(JSON.parse(res));
                    // console.log('====================================');
                    Constants.LogIn_User = JSON.parse(res);
                }else{
                    Constants.LogIn_User = ""
                }

            });
        }
    })
    const setUserLoginData = userLogin => {
        if (userLogin !== null) {
            // Constants.LogIn_User = JSON.stringify(userLogin)
            AsyncStorage.setItem('userLogin', JSON.stringify(userLogin));
            setUserLogin(userLogin);
        } else {
            setUserLogin(null);
            AsyncStorage.setItem('userLogin', '');
        }
    };
    return (
        <UserContext.Provider
            value={{
                userLogin: userLogin,
                setUserLogin: setUserLoginData,
            }}>
            {children}
        </UserContext.Provider>
    );
};