import React, { createContext, useState, useEffect } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Constants} from "../../constant/Constants";

export const UserBalanceContext = createContext({
    setUserBalance: () => { },
    userBalance: 0,
});

export const UserBalanceProvider = ({ children }) => {
    const [userBalance, setUserBalance] = useState(0);
    useEffect(() => {
        AsyncStorage.getItem('userBalance').then((res) => {
            if (res != null) {
                setUserBalance(parseInt(res))
                Constants.LoginUserBalance =res;
            }
        });
    })
    const setUserBalanceData = userBalance => {
        if (userBalance !== null) {
           
           Constants.LoginUserBalance = JSON.stringify(userBalance)
            AsyncStorage.setItem('userBalance', JSON.stringify(userBalance));
            setUserBalance(userBalance);
        } else {
            setUserBalance(0);
            AsyncStorage.setItem('userBalance', 0);
        }
    };
    return (
        <UserBalanceContext.Provider
            value={{
                userBalance: userBalance,
                setUserBalance: setUserBalanceData,
            }}>
            {children}
        </UserBalanceContext.Provider>
    );
};