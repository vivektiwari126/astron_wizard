import { Constants } from "../../../src/constant/Constants";
import { ToastAndroid } from "react-native";

//  -------------------------------Paytm Calling API Start ------------------------------

export const generateToken = async (orderData) => {
    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    var raw = JSON.stringify(orderData);

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow',
    };

    return await fetch(Constants.API_URL, requestOptions)
        .then(response => response.json())
        .then(result => {
            //   console.log("result", result)
            //  alert("---result---" + JSON.stringify(result))
            return result;
        })
        .catch(error => {
            console.log('error', error);
            // alert("---error---" + JSON.stringify(error))

        });

};


//  -------------------------------Paytm Calling API End ------------------------------




//  -------------------------------Knowlarity Calling API Start ------------------------------


export const knowlarityCall = async (k_number, agent_number, customer_number, caller_id, total_call_duration) => {
    var myHeaders = new Headers();
    myHeaders.append("x-api-key", Constants.knowlarityKEY);
    myHeaders.append("Authorization", Constants.knowlarityAuthToken);
    myHeaders.append("contentType", "application/json");

    var raws = JSON.stringify({
        "k_number": k_number,
        "agent_number": agent_number,
        "customer_number": customer_number,
        "caller_id": caller_id,
        "additional_params": {
            "total_call_duration": total_call_duration
        }
    });
    var requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: raws,
        redirect: "follow"
    };

    return await fetch(Constants.knowlarityApi, requestOptions)
        .then(response => response.json())
        .then(result => {
              console.log("Success Dt ---", result);
            ToastAndroid.show(result.success.message, ToastAndroid.LONG);
        }).catch(error => {
            ToastAndroid.show(error.error.message, ToastAndroid.LONG);
            console.log("error Dt ---", error)
        })
}

export const inputValFlag = (textData) => {

    if (textData != null && textData != undefined && textData != '') {
        return true;
    } else {
        return false;
    }
}

export const validateEmail = (email) => {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}


//  -------------------------------Knowlarity Calling API End ------------------------------


//  -------------------------------Sign Up API End ------------------------------


export const astronSign = async (jsonData) => {
    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    var raw = JSON.stringify(jsonData);

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow',
    };

    return await fetch(Constants.SIGNUP_URL, requestOptions)
        .then(response => response.json())
        .then(result => {
            // console.log("result", result)
            return result;
        })
        .catch(error => {
            console.log('error', error);
        });

};


export const astronLogin = async (jsonData) => {
    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    var raw = JSON.stringify(jsonData);
    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow',
    };

    return await fetch(Constants.LOGIN_URL, requestOptions)
        .then(response => response.json())
        .then(result => {
            //  console.log("result", result);
            return result;
        })
        .catch(error => {
            console.log('error', error);
        });
};

export const astronFinalPayment = async (jsonData) => {
    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    var raw = JSON.stringify(jsonData);
    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow',
    };

    return await fetch(Constants.PAYMENT_CONFIRM_URL, requestOptions)
        .then(response => response.json())
        .then(result => {
            //    console.log("result", result)
            return result;
        })
        .catch(error => {
            console.log('error', error);
        });
};


export const astronGetAstrologerList = async (jsonData) => {
    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow',
    };

    return await fetch(Constants.ASTROLOGER_GET_URL, requestOptions)
        .then(response => response.json())
        .then(result => {
            //  console.log("result", result)
            return result;
        })
        .catch(error => {
            console.log('error', error);
        });
};


export const astronCustBalance = async (jsonData) => {
    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    var raw = JSON.stringify(jsonData);
    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow',
    };

    return await fetch(Constants.CUSTOMER_BALANCE_URL, requestOptions)
        .then(response => response.json())
        .then(result => {
            //   console.log("result", result)
            return result;
        })
        .catch(error => {
            console.log('error', error);
        });
};


export const astronCheckAstroStatus = async (jsonData) => {
    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    var raw = JSON.stringify(jsonData);
    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow',
    };

    return await fetch(Constants.ASTROLOGER_CHECKSTATUS_URL, requestOptions)
        .then(response => response.json())
        .then(result => {
            //   console.log("result", result)
            return result;
        })
        .catch(error => {
            console.log('error', error);
        });
};



export const astronCustomerCallInitiate = async (jsonData) => {
    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    var raw = JSON.stringify(jsonData);
    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow',
    };

    return await fetch(Constants.CUSTOMER_CALL_INITIATE_URL, requestOptions)
        .then(response => response.json())
        .then(result => {
            //   console.log("result", result)
            return result;
        })
        .catch(error => {
            console.log('error', error);
        });
};

export const getAstronRechargeAmount = async (jsonData) => {
    var requestOptions = {
        method: 'GET',
        redirect: 'follow',
    };

    return await fetch(Constants.ASTRON_RECHARGE_AMOUNT_URL, requestOptions)
        .then(response => response.json())
        .then(result => {
            //   console.log("result", result)
            return result;
        })
        .catch(error => {
            console.log('error', error);
        });
};

export const astronFreeRechargeApi = async (jsonData) => {
    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    var raw = JSON.stringify(jsonData);
    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow',
    };

    return await fetch(Constants.FREE_RECHARGE_ASTRON_URL, requestOptions)
        .then(response => response.json())
        .then(result => {
            //   console.log("result", result)
            return result;
        })
        .catch(error => {
            console.log('error', error);
        });
};
