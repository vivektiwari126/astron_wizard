import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import React, { useContext } from 'react';
import { enableScreens } from 'react-native-screens';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AsyncStorage from '@react-native-async-storage/async-storage';
import HomeScreen from "../screen/home/HomeScreen";
import ProfileScreen from "../screen/profile/ProfileScreen";
import RechargeScreen from "../screen/recharge/RechargeScreen";
import { Images } from "../styles/Images";
import { Label_Input_Data } from "../label/Label_Input_Data";
const Tab = createBottomTabNavigator();
const AstronBottomNavigator = (props) => {
    return (
        <Tab.Navigator
            initialRouteName={Label_Input_Data.BottomNavigateData.Home}
            tabBarOptions={{
                activeTintColor: 'red',
                inactiveTintColor: "grey",     //'#17a2b8',
                style: {
                    paddingVertical: 7, backgroundColor: '#f5f5f5',
                    height: 30, width: '100%',
                },
            }}
        >
            <Tab.Screen
                name={Label_Input_Data.BottomNavigateData.Home}
                component={HomeScreen}
                options={{
                    tabBarLabel: (Label_Input_Data.BottomNavigateData.Home),
                    headerStyle: {
                        backgroundColor: '#f4511e',

                    },
                    headerTintColor: '#fff',
                    headerTitleStyle: {
                        fontWeight: 'bold',
                        marginLeft: '48%'
                    },
                    headerLeft: () => (
                        <TouchableOpacity onPress={()=>{props.navigation.navigate("Setting")}}>
                            <View>
                                <Image tintColor="#fff" source={Images.menu} resizeMode="center"
                                    style={{ width: 60, height: 20 }} />

                            </View>
                        </TouchableOpacity>
                    ),
                    headerRight: () => (
                        <TouchableOpacity onPress={()=>{props.navigation.navigate("Recharge")}}>
                            <View>
                                <Image tintColor="#fff" source={Images.digitalWallet} resizeMode="center"
                                    style={{ width: 80, height: 32 }} />

                            </View>
                        </TouchableOpacity>

                    ),
                    tabBarIcon: ({ color, size }) => (
                        <Image source={Images.home} resizeMode={styles.imageResizeMode}
                            tintColor={color} style={styles.imageStyle} />
                    ),
                }}
            />

            <Tab.Screen
                name={Label_Input_Data.BottomNavigateData.Recharge}
                component={RechargeScreen}
                options={{
                    tabBarLabel: (Label_Input_Data.BottomNavigateData.Recharge),
                    headerStyle: {
                        backgroundColor: '#f4511e',
                    },
                    headerTintColor: '#fff',
                    headerTitleStyle: {
                        fontWeight: 'bold',
                        marginLeft: '48%'
                    },
                    headerLeft: () => (
                        <TouchableOpacity onPress={()=>{props.navigation.navigate("Setting")}}>
                            <View>
                                <Image tintColor="#fff" source={Images.menu} resizeMode="center"
                                    style={{ width: 60, height: 20 }} />

                            </View>
                        </TouchableOpacity>
                    ),
                    headerRight: () => (
                        <TouchableOpacity onPress={()=>{props.navigation.navigate("Recharge")}}>
                            <View>
                                <Image tintColor="#fff" source={Images.digitalWallet} resizeMode="center"
                                    style={{ width: 80, height: 32 }} />

                            </View>
                        </TouchableOpacity>

                    ),
                    tabBarIcon: ({ color, size }) => (
                        <Image source={Images.mobilemoney} resizeMode={styles.imageResizeMode}
                            tintColor={color} style={styles.imageStyle} />
                    ),

                }}
            />
            <Tab.Screen
                name={Label_Input_Data.BottomNavigateData.Profile}
                component={ProfileScreen}
                options={{
                    tabBarLabel: (Label_Input_Data.BottomNavigateData.Profile),
                    headerStyle: {
                        backgroundColor: '#f4511e',


                    },
                    headerTintColor: '#fff',
                    headerTitleStyle: {
                        fontWeight: 'bold',
                        marginLeft: '48%'
                    },
                    headerLeft: () => (
                        <TouchableOpacity onPress={()=>{props.navigation.navigate("Setting")}}>
                            <View>
                                <Image tintColor="#fff" source={Images.menu} resizeMode="center"
                                    style={{ width: 60, height: 20 }} />

                            </View>
                        </TouchableOpacity>
                    ),
                    headerRight: () => (
                        <TouchableOpacity onPress={()=>{props.navigation.navigate("Recharge")}}>
                            <View>
                                <Image tintColor="#fff" source={Images.digitalWallet} resizeMode="center"
                                    style={{ width: 80, height: 32 }} />

                            </View>
                        </TouchableOpacity>

                    ),
                    tabBarIcon: ({ color, size }) => (
                        <Image source={Images.user} tintColor={color}
                            resizeMode={styles.imageResizeMode}
                            style={styles.imageStyle} />
                    ),
                }}
            />
        </Tab.Navigator>
    )

}


const styles = StyleSheet.create({
    imageResizeMode: "center",
    imageStyle: { width: '50%', height: '80%' },


});


export default AstronBottomNavigator;