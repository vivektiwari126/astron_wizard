import { View, Text, Image, Linking, Platform } from 'react-native';
import React, { useRef, useContext, useEffect, useState } from 'react';
import { NavigationContainer, DarkTheme, DefaultTheme, } from '@react-navigation/native';
import { enableScreens } from 'react-native-screens';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
//import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { SignUpPage } from "../screen/signup/SignUpPage";
import { AstronLoginPage } from "../screen/login/AstronLoginPage";
import { Label_Input_Data } from "../label/Label_Input_Data";
import AstronBottomNavigator from "../navigator/AstronBottomNavigator";
import SettingScreen from "../screen/setting/SettingScreen";
import { ConfigFirebase } from "../component/ConfigFirebase";
import { UserContext } from "../services/logincontext/UserContext";
import { Constants } from '../constant/Constants';
import { inputValFlag } from "../services/paytmservice/PaytmService";

//import { createDrawerNavigator } from "@react-navigation/drawer";
const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();
//const Drawer = createDrawerNavigator();
enableScreens();
const SignUpStack = (props) => {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false, }} initialRouteName={Label_Input_Data.NavigationData.Login}>
            <Stack.Screen name={Label_Input_Data.NavigationData.Login} component={AstronLoginPage} />
            <Stack.Screen name={Label_Input_Data.NavigationData.SignUp} component={SignUpPage} />
        </Stack.Navigator>
    )
}

const LoginStack = (props) => {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false, }} initialRouteName={Label_Input_Data.NavigationData.Login}>
            <Stack.Screen name="Home" component={AstronBottomNavigator} props />
            <Stack.Screen name="Setting" component={SettingScreen} />
        </Stack.Navigator>
    )
}


const Routes = (props) => {
    const navigationRef = useRef();
    const { setUserLogin, userLogin } = useContext(UserContext);
    ConfigFirebase.ConfigFirebase;
    const getFCMToken = ConfigFirebase.checkFireBaseInitialize();
    return (
        < NavigationContainer theme={DefaultTheme}
            ref={navigationRef}
        >
            {userLogin !== null && userLogin !== '' ? <LoginStack props {...userLogin} /> : <SignUpStack props />}

        </ NavigationContainer>
    )
}


export default Routes;