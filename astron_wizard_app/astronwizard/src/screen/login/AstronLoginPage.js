import React, { Component, useContext, useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, TextInput, Image, ToastAndroid, Dimensions, ScrollView, TouchableOpacity, SafeAreaView, Platform, Linking, ActivityIndicator } from 'react-native';
import LoginComponent from "../../component/LoginComponent";
import { Images } from "../../styles/Images";
import { Fonts } from "../../styles/Fonts";
import database from '@react-native-firebase/database'
import { inputValFlag, validateEmail } from "../../services/paytmservice/PaytmService";
import { Label_Input_Data } from "../../label/Label_Input_Data";
import { Constants } from "../../constant/Constants";
import { astronLogin } from "../../services/paytmservice/PaytmService";
import { UserContext } from '../../services/logincontext/UserContext'
import { GoogleSignin, GoogleSigninButton, statusCodes, } from '@react-native-community/google-signin';
import { Button, ThemeProvider, Divider, Input, Icon, SocialIcon } from 'react-native-elements';
const screenHeight = Dimensions.get('window').height
export class AstronLoginPage extends Component {

    static contextType = UserContext
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            emailId: '',
            password: '',
        }
    }
    astronLogin = async () => {
        const { setUserLogin, userLogin } = this.context
        var jsonData = {};
        if (inputValFlag(this.state.emailId) && validateEmail(this.state.emailId)) {
            jsonData[Label_Input_Data.Json_Input_Field.emailId] = this.state.emailId.trim()
        } else {
            ToastAndroid.show(Label_Input_Data.MESSAGE.emailErrMsg, ToastAndroid.LONG);
            return true;
        }
        if (inputValFlag(this.state.password)) {
            jsonData[Label_Input_Data.Json_Input_Field.password] = this.state.password.trim()
        } else {
            ToastAndroid.show(Label_Input_Data.MESSAGE.passErrMsg, ToastAndroid.LONG);
            return true;
        }

        const astronLoginResp = await astronLogin(jsonData)
        if (astronLoginResp.status == 1) {
            ToastAndroid.show(astronLoginResp.msg, ToastAndroid.LONG);
            Constants.LogIn_User = astronLoginResp;
            setUserLogin(astronLoginResp);
            this.props.navigation.navigate("Home");
        } else {
            ToastAndroid.show(astronLoginResp.msg, ToastAndroid.LONG);
        }


    }
    render() {
        const { emailId, password } = this.state
        return (
            <View style={{ flex: 1 }} >
                <ScrollView style={{ height: screenHeight }}>
                    <View>
                        <View style={{ height: screenHeight / 4, marginBottom: screenHeight / 18 }}>
                            <Image source={Images.logo} resizeMode="center" style={styles.imageStyle} />
                        </View>
                        <View>
                            <Input
                                label={Label_Input_Data.AstroLoginData.emailLabel}
                                labelStyle={styles.inputLabelStyle}
                                placeholder={Label_Input_Data.AstroLoginData.emailPlaceHolder}
                                errorStyle={styles.errorColorStyle}
                                errorMessage=''
                                value={emailId}
                                name="emailId"
                                onChangeText={(emailId) => { this.setState({ emailId }) }}
                                containerStyle={{ color: 'green' }}
                                leftIcon={
                                    <Icon
                                        name='mail'
                                        size={styles.iconSize}
                                        style={styles.iconStyle}
                                        color='black'
                                    />
                                }
                            />
                        </View>
                        <View style={{ zIndex: 2 }}>
                            <Input
                                label={Label_Input_Data.AstroLoginData.passLabel}
                                placeholder={Label_Input_Data.AstroLoginData.passPlaceHolder}
                                labelStyle={styles.inputLabelStyle}
                                errorStyle={styles.errorColorStyle}
                                errorMessage=''
                                name="password"
                                value={password}
                                secureTextEntry={true}
                                onChangeText={(password) => { this.setState({ password }) }}
                                leftIcon={
                                    <Icon
                                        name='https'
                                        size={styles.iconSize}
                                        style={styles.iconStyle}
                                        color='black'
                                    />
                                }
                            />
                        </View>
                        <View>
                            <Button
                                title={Label_Input_Data.AstroLoginData.loginButton}
                                loading={false}
                                loadingProps={styles.loadingPropsStyle}
                                buttonStyle={styles.buttonStyle}
                                titleStyle={{ fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: 16, }}
                                containerStyle={styles.buttonContStyle}
                                onPress={() => {
                                    //
                                    this.astronLogin();
                                }}
                            />
                        </View>
                        {/* <View>
                            <SocialIcon title={Label_Input_Data.AstroLoginData.fbLabel}
                                button type='facebook' onPress={() => alert(Label_Input_Data.AstroLoginData.fbLabel)}
                                style={styles.socialButtonStyle}
                            />
                        </View>
                        <View>
                            <SocialIcon title={Label_Input_Data.AstroLoginData.googleLabel}
                                button type='google' style={styles.socialButtonStyle} onPress={() => alert(Label_Input_Data.AstroLoginData.googleLabel)} />
                        </View> */}
                        <View>
                            <Button
                                title={Label_Input_Data.AstroLoginData.signUpButton}
                                loading={false}
                                loadingProps={styles.loadingPropsStyle}
                                buttonStyle={[styles.buttonStyle, { backgroundColor: 'grey' }]}
                                titleStyle={{ fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: 16, }}
                                containerStyle={styles.buttonContStyle}
                                onPress={() => this.props.navigation.navigate(Label_Input_Data.NavigationData.SignUp)}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    loadingPropsStyle: { size: 'small', color: 'white' },
    buttonStyle: {
        backgroundColor: 'rgba(111, 202, 186, 1)',
        borderRadius: 15,
        padding: 12
    },
    imageStyle: { height: screenHeight / 3.5, width: '100%' },
    iconSize: 24,
    iconStyle: {
        padding: 5,
    },
    errorColorStyle: {
        color: 'red',
    },
    socialButtonStyle: {
        borderRadius: 15, height: 48,
        marginHorizontal: 10,
    },
    buttonContStyle: {
        marginHorizontal: 12,
        height: 48,
        marginVertical: 5,
    },
    inputLabelStyle: {
        color: 'grey', fontSize: 16, paddingLeft: 10,
        fontFamily: Fonts.Roboto.Roboto_Black
    },
});


//  "react-native-text-input-interactive": "^0.1.3"