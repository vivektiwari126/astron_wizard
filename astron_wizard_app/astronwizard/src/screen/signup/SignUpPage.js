import React, { Component, useContext, useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, TextInput, Dimensions, ToastAndroid, ScrollView, TouchableOpacity, SafeAreaView, Platform, Linking, ActivityIndicator } from 'react-native';
import LoginComponent from "../../component/LoginComponent";
import { Images } from "../../styles/Images";
import { Fonts } from "../../styles/Fonts";
import { inputValFlag ,validateEmail,astronSign,astronFreeRechargeApi} from "../../services/paytmservice/PaytmService";
import { Label_Input_Data } from "../../label/Label_Input_Data";
const screenHeight = Dimensions.get('window').height

import { Button, ThemeProvider, Divider, Input, Icon, SocialIcon, Image } from 'react-native-elements';
export class SignUpPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            fullName: "",
            emailId: '',
            mobileNo: '',
            password: '',
            confirmPassword: '',

        }
    }

    astronSignUp =async () => {

        var jsonData = {};
        if (inputValFlag(this.state.fullName)) {
            jsonData[Label_Input_Data.Json_Input_Field.fullName] = this.state.fullName.trim()
        } else {
            ToastAndroid.show(Label_Input_Data.MESSAGE.fullNameErrMsg, ToastAndroid.LONG);
            return true;
        }
        if (inputValFlag(this.state.emailId) && validateEmail(this.state.emailId)) {
            jsonData[Label_Input_Data.Json_Input_Field.emailId] = this.state.emailId.trim()
        } else {
            ToastAndroid.show(Label_Input_Data.MESSAGE.emailErrMsg, ToastAndroid.LONG);
            return true;
        }

        if (inputValFlag(this.state.mobileNo)) {
            jsonData[Label_Input_Data.Json_Input_Field.mobileNo] = this.state.mobileNo.trim()
        } else {
            ToastAndroid.show(Label_Input_Data.MESSAGE.mobileNoErrMsg, ToastAndroid.LONG);
            return true;
        }

        if (inputValFlag(this.state.password)) {
            jsonData[Label_Input_Data.Json_Input_Field.password] = this.state.password.trim()
        } else {
            ToastAndroid.show(Label_Input_Data.MESSAGE.passErrMsg, ToastAndroid.LONG);
            return true;
        }
        if (!inputValFlag(this.state.confirmPassword)) {
            
            ToastAndroid.show(Label_Input_Data.MESSAGE.confirmPassErrMsg, ToastAndroid.LONG);
            return true;
        }else{
            jsonData[Label_Input_Data.Json_Input_Field.confirmPassword] = this.state.confirmPassword.trim()
            
        }
        const astronSignResp = await astronSign(jsonData);
        if(astronSignResp.status==1)
        {
            const astronFreeRechResp = await astronFreeRechargeApi(jsonData);
            if(astronFreeRechResp.status==1)
            {
                ToastAndroid.show(astronSignResp.msg, ToastAndroid.LONG);
                this.props.navigation.navigate("Login");
            }
        }else{
            ToastAndroid.show(astronSignResp.msg, ToastAndroid.LONG);
        }
    }

    render() {
        const { fullName, emailId, mobileNo, password, confirmPassword } = this.state
        return (

            <View style={{ flex: 1 }} >
                <ScrollView style={{ height: screenHeight }}>
                    <View>
                        <View style={{ height: screenHeight / 4, marginBottom: screenHeight / 18 }}>
                            <Image source={Images.logo} resizeMode="center" style={styles.imageStyle} />
                        </View>
                        <View>
                            <Input
                                label={Label_Input_Data.SignUpData.nameLabel}
                                labelStyle={styles.inputLabelStyle}
                                placeholder={Label_Input_Data.SignUpData.namePlaceHolder}
                                errorStyle={styles.errorColorStyle}
                                errorMessage=''
                                value={fullName}
                                name="fullName"
                                onChangeText={(fullName) => { this.setState({ fullName }) }}
                                containerStyle={{ color: 'green' }}
                                leftIcon={
                                    <Icon
                                        name='person'
                                        size={styles.iconSize}
                                        style={styles.iconStyle}
                                        color='black'
                                    />
                                }
                            />
                        </View>
                        <View>
                            <Input
                                label={Label_Input_Data.SignUpData.emailLabel}
                                labelStyle={styles.inputLabelStyle}
                                placeholder={Label_Input_Data.SignUpData.emailPlaceHolder}
                                errorStyle={styles.errorColorStyle}
                                errorMessage=''
                                value={emailId}
                                name="emailId"
                                onChangeText={(emailId) => { this.setState({ emailId }) }}
                                containerStyle={{ color: 'green' }}
                                leftIcon={
                                    <Icon
                                        name='mail'
                                        size={styles.iconSize}
                                        style={styles.iconStyle}
                                        color='black'
                                    />
                                }
                            />
                        </View>
                        <View>
                            <Input
                                label={Label_Input_Data.SignUpData.mobileNoLabel}
                                labelStyle={styles.inputLabelStyle}
                                placeholder={Label_Input_Data.SignUpData.mobileNoPlaceHolder}
                                errorStyle={styles.errorColorStyle}
                                errorMessage=''
                                value={mobileNo}
                                name="mobileNo"
                                onChangeText={(mobileNo) => { this.setState({ mobileNo }) }}
                                containerStyle={{ color: 'green' }}
                                leftIcon={
                                    <Icon
                                        name='call'
                                        size={styles.iconSize}
                                        style={styles.iconStyle}
                                        color='black'
                                    />
                                }
                            />
                        </View>
                        <View>
                            <Input
                                label={Label_Input_Data.SignUpData.passLabel}
                                placeholder={Label_Input_Data.SignUpData.passPlaceHolder}
                                labelStyle={styles.inputLabelStyle}
                                errorStyle={styles.errorColorStyle}
                                errorMessage=''
                                value={password}
                                name="password"
                                secureTextEntry={true}
                                onChangeText={(password) => { this.setState({ password }) }}
                                leftIcon={
                                    <Icon
                                        name='https'
                                        size={styles.iconSize}
                                        style={styles.iconStyle}
                                        color='black'
                                    />
                                }
                            />
                        </View>
                        <View>
                            <Input
                                label={Label_Input_Data.SignUpData.confirmPassLabel}
                                placeholder={Label_Input_Data.SignUpData.confirmPassPlaceHolder}
                                labelStyle={styles.inputLabelStyle}
                                errorStyle={styles.errorColorStyle}
                                errorMessage=''
                                value={confirmPassword}
                                secureTextEntry={true}
                                name="confirmPassword"
                                onChangeText={(confirmPassword) => { this.setState({ confirmPassword }) }}
                                leftIcon={
                                    <Icon
                                        name='https'
                                        size={styles.iconSize}
                                        style={styles.iconStyle}
                                        color='black'
                                    />
                                }
                            />
                        </View>
                        <View>
                            <Button
                                title={Label_Input_Data.AstroLoginData.signUpButton}
                                loading={false}
                                loadingProps={styles.loadingPropsStyle}
                                buttonStyle={[styles.buttonStyle, { backgroundColor: 'grey' }]}
                                titleStyle={{ fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: 16, }}
                                containerStyle={styles.buttonContStyle}
                                onPress={() => this.astronSignUp()}
                            />
                        </View>
                        <TouchableOpacity onPress={() => { this.props.navigation.navigate("Login"); }}>
                            <View style={{ marginTop: 10, marginBottom: 5 }}>
                                <Text style={{
                                    textAlign: 'center',
                                    fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: 18, textDecorationStyle: "solid",
                                    textDecorationColor: "#000", textDecorationLine: "underline",
                                }}> Back </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    loadingPropsStyle: { size: 'small', color: 'white' },
    buttonStyle: {
        backgroundColor: 'rgba(111, 202, 186, 1)',
        borderRadius: 15,
        padding: 12
    },
    imageStyle: { height: screenHeight / 3.5, width: '100%' },
    iconSize: 24,
    iconStyle: {
        padding: 5,
    },
    errorColorStyle: {
        color: 'red',
    },
    buttonContStyle: {
        marginHorizontal: 12,
        height: 48,
        marginVertical: 5,
    },
    inputLabelStyle: { color: 'grey', fontSize: 16, paddingLeft: 10, fontFamily: Fonts.Roboto.Roboto_Black },
});


//  "react-native-text-input-interactive": "^0.1.3"