import React, { Component, useContext, useState, useEffect } from 'react';
import { View, StyleSheet, FlatList, ScrollView, ToastAndroid, TextInput, Image, TouchableOpacity, Platform, Linking, ActivityIndicator } from 'react-native';
import { Text, Card, Button, Icon } from 'react-native-elements';
import { Images } from "../../styles/Images";
import { Fonts } from "../../styles/Fonts";
import { Label_Input_Data } from "../../label/Label_Input_Data";
import AllInOneSDKManager from 'paytm_allinone_react-native';
import { generateToken, inputValFlag, astronFinalPayment, astronCustBalance,getAstronRechargeAmount } from "../../services/paytmservice/PaytmService";
import { Constants } from "../../constant/Constants";
import { UserBalanceContext } from "../../services/logincontext/UserBalanceContext";
class RechargeScreen extends Component {
    static contextType = UserBalanceContext
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            selectedAmount: '',
            userData: Constants.LogIn_User,
            customerBalance: 0,
            astronRecharageList:[]
        }
    }
    componentDidMount() {
        //   console.log("RechargeScreen----", this.state.userData.data.emailId)
        this.unsubscribe = this.props.navigation.addListener('focus', () => {
        this.astronCustomerBalance()
        this.getAstroRechargeData();
        })
    }
    componentWillUnmount() {
        this.unsubscribe();
    }

    getAstroRechargeData = async () => {
        const astroRechargeDataResp = await getAstronRechargeAmount();
        if (astroRechargeDataResp.status == 1) {
            ToastAndroid.show(astroRechargeDataResp.msg, ToastAndroid.LONG);
            this.setState({ astronRecharageList: astroRechargeDataResp.data });

        } else {
            ToastAndroid.show(astroRechargeDataResp.msg, ToastAndroid.LONG);
        }


    }




    paytmPayment = async () => {
        var jsonData = {};
        if (!inputValFlag(this.state.selectedAmount)) {
            ToastAndroid.show("Amount is required.", ToastAndroid.LONG);
            return 1;
        }
        const orderId = Math.floor(Math.random() * 10000) + 1;
        jsonData.orderId = orderId.toString();
        jsonData.amt = this.state.selectedAmount.toString();
        jsonData.customerId = "CUST_" + this.state.userData.data.mobileNo;
        jsonData.emailId = this.state.userData.data.emailId;
        jsonData.mobileNo = this.state.userData.data.mobileNo;
        const getToken = await generateToken(jsonData);
        try {
            AllInOneSDKManager.startTransaction(
                orderId.toString(),
                Constants.MID,
                getToken.body.txnToken,
                this.state.selectedAmount.toString(),
                Constants.CALL_BACK_URL + orderId,
                false, // isStaging for use true,
                true,//  appInvokeRestricted,
                Constants.URL_SCHEME,
            ).then((result) => {

                if (result.errorCode != "227") {
                    var jsonPayData = {};
                    jsonPayData.orderId = orderId.toString();
                    jsonPayData.amt = this.state.selectedAmount.toString();
                    jsonPayData.emailId = this.state.userData.data.emailId;
                    jsonPayData.mobileNo = this.state.userData.data.mobileNo;
                    jsonPayData.paymentData = result;
                    if (result.STATUS !== "TXN_FAILURE") {
                        this.confirmPayment(jsonPayData);
                    }
                    console.log("AllInOneSDKManager-----result", result)
                    ToastAndroid.show(result.STATUS, ToastAndroid.LONG);
                    this.setState({ selectedAmount: '' })
                } else {
                    ToastAndroid.show("Payment not successfull.", ToastAndroid.LONG);
                    this.setState({ selectedAmount: '' })
                }
            })
                .catch((err) => {
                    console.log("AllInOneSDKManager-----err--", err)
                    //     ToastAndroid.show(err, ToastAndroid.LONG);
                });

        } catch (err) {
            // ToastAndroid.show(err, ToastAndroid.LONG);
        };

    }

    confirmPayment = async (jsonPayData) => {
        const payDataResp = await astronFinalPayment(jsonPayData);
        if (payDataResp.status == 1) {
            ToastAndroid.show(payDataResp.msg, ToastAndroid.LONG);
            this.astronCustomerBalance()
        } else {
            ToastAndroid.show(payDataResp.msg, ToastAndroid.LONG);
        }
    }

    astronCustomerBalance = async () => {
        var jsonData = {};
        jsonData.emailId = this.state.userData.data.emailId;
        const astronCustomerBalanceResp = await astronCustBalance(jsonData);
        if (astronCustomerBalanceResp.status == 1) {
            ToastAndroid.show(astronCustomerBalanceResp.msg, ToastAndroid.LONG);
            var arrayAmount = 0;
            astronCustomerBalanceResp.data.map((item, index) => {
                console.log('====================================');
                console.log(item.amount,"totl moint ",arrayAmount);
                console.log('====================================');
                return arrayAmount = arrayAmount + Number(item.amount)
            })
            const { setUserBalance, userBalance } = this.context;
            Constants.LoginUserBalance = arrayAmount;
            setUserBalance(arrayAmount);
            this.setState({ customerBalance: arrayAmount })
        } else {
            // ToastAndroid.show(astronCustomerBalanceResp.msg, ToastAndroid.LONG);
        }
    }
    render() {
        const { selectedAmount, customerBalance } = this.state
        return (
            <ScrollView>
                <View style={{ height: 170, }}>
                    <View style={{ height: 120, }}>
                        <Text style={styles.walletTextStyle}>{Label_Input_Data.RechargeInput_Data.addMoneyWalletLabel}</Text>
                        <Text style={styles.availableBalanceStyle}>{Label_Input_Data.RechargeInput_Data.availableBalanceLabel}  {" ₹  " + customerBalance} </Text>
                    </View>
                    <View style={styles.inputAmountStyle}>
                        <TextInput style={{ paddingLeft: 12, fontFamily: Fonts.Roboto.Roboto_Bold, height: 45, width: 244, backgroundColor: '#ffff', }}
                            value={!inputValFlag(selectedAmount) ? "" : "₹ " + selectedAmount}
                            name="selectedAmount"
                            onChangeText={(selectedAmount) => { this.setState({ selectedAmount }) }}
                            placeholder={Label_Input_Data.RechargeInput_Data.enterAmountPlaceHolder}
                        />
                        <Button
                            title={Label_Input_Data.RechargeInput_Data.proceedButton}
                            loading={false}
                            loadingProps={styles.loadingPropsStyle}
                            buttonStyle={styles.proceedButton}
                            titleStyle={{ fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: 12, }}
                            onPress={() => { this.paytmPayment() }}
                        />
                    </View>
                </View>
                <View style={{ flexDirection: 'row', paddingTop: 50, flexWrap: 'wrap', marginBottom: 10, }}>
                    {this.state.astronRecharageList.map((item, index) => (
                        <View key={index} style={{ width: '50%' }}>
                            <Button
                                title={item.amountRecharge}
                                loading={false}
                                loadingProps={styles.loadingPropsStyle}
                                buttonStyle={[styles.buttonStyle, { backgroundColor: (selectedAmount == item.amount ? "#f4511e" : "grey") }]}
                                titleStyle={{ fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: 20, }}
                                containerStyle={styles.buttonContStyle}
                                onPress={() => { this.setState({ selectedAmount: item.amount }) }}
                            />
                        </View>
                    ))
                    }

                </View>
            </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    walletTextStyle: { padding: 20, fontFamily: Fonts.Roboto.Roboto_Bold, textAlign: "center", fontSize: 19, color: 'green' },
    availableBalanceStyle: { paddingBottom: 10, fontFamily: Fonts.Roboto.Roboto_Medium, textAlign: "center", fontSize: 16, color: 'green' },
    inputAmountStyle: {
        height: 50, marginHorizontal: 20,
        borderRadius: 6,
        padding: 2.5,
        borderWidth: 1, borderColor: 'grey',
        flexDirection: 'row'
    },
    buttonContStyle: {
        marginHorizontal: 8,
        marginTop: 20,
    },
    buttonStyle: {
        backgroundColor: 'grey',
        borderRadius: 35,
        padding: 6,
        height: 55,
    },
    proceedButton: {
        backgroundColor: 'grey',
        borderRadius: 5,
        padding: 5,
        width: 120,
        height: 42,

    }
})


export default RechargeScreen;