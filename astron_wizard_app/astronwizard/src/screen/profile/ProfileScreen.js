import React, { Component, useContext, useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, TextInput, Dimensions, ToastAndroid, ScrollView, TouchableOpacity, SafeAreaView, Platform, Linking, ActivityIndicator } from 'react-native';
import LoginComponent from "../../component/LoginComponent";
import { Images } from "../../styles/Images";
import { Fonts } from "../../styles/Fonts";
import { inputValFlag } from "../../services/paytmservice/PaytmService";
import { Label_Input_Data } from "../../label/Label_Input_Data";
import { Constants } from "../../constant/Constants";
const screenHeight = Dimensions.get('window').height
import { Button, ThemeProvider, Divider, Input, Icon, SocialIcon, Image } from 'react-native-elements';
class ProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            fullName: "",
            emailId: '',
            mobileNo: '',
            password: '',
            confirmPassword: '',
            userData: Constants.LogIn_User,
        }
    }

    componentDidMount() {
        const userData = this.state.userData;
        this.setState({
            fullName: userData.data.fullName, emailId: userData.data.emailId,
            mobileNo: userData.data.mobileNo, userData
        })
        console.log("ProfileScreen----", this.state.userData.data)
    }

    astronUpdateProfile = () => {

    }

    render() {
        const { fullName, emailId, mobileNo, password, confirmPassword } = this.state
        return (

            <View style={{ flex: 1 }} >
                <ScrollView style={{ height: screenHeight }}>
                    <View>
                        <View style={{ height: screenHeight / 4, marginTop: screenHeight / 40 }}>
                            <Image source={Images.user} tintColor="grey" resizeMode="center" style={styles.imageStyle} />
                        </View>
                        <View>
                            <Input
                                label={Label_Input_Data.SignUpData.nameLabel}
                                labelStyle={styles.inputLabelStyle}
                                placeholder={Label_Input_Data.SignUpData.namePlaceHolder}
                                errorStyle={styles.errorColorStyle}
                                errorMessage=''
                                value={fullName}
                                name="fullName"
                                editable={false}
                                onChangeText={(fullName) => { this.setState({ fullName }) }}
                                containerStyle={{ color: 'green' }}
                                leftIcon={
                                    <Icon
                                        name='person'
                                        size={styles.iconSize}
                                        style={styles.iconStyle}
                                        color='black'
                                    />
                                }
                            />
                        </View>
                        <View>
                            <Input
                                label={Label_Input_Data.SignUpData.emailLabel}
                                labelStyle={styles.inputLabelStyle}
                                placeholder={Label_Input_Data.SignUpData.emailPlaceHolder}
                                errorStyle={styles.errorColorStyle}
                                errorMessage=''
                                value={emailId}
                                editable={false}
                                name="emailId"
                                onChangeText={(emailId) => { this.setState({ emailId }) }}
                                containerStyle={{ color: 'green' }}
                                leftIcon={
                                    <Icon
                                        name='mail'
                                        size={styles.iconSize}
                                        style={styles.iconStyle}
                                        color='black'
                                    />
                                }
                            />
                        </View>
                        <View>
                            <Input
                                label={Label_Input_Data.SignUpData.mobileNoLabel}
                                labelStyle={styles.inputLabelStyle}
                                placeholder={Label_Input_Data.SignUpData.mobileNoPlaceHolder}
                                errorStyle={styles.errorColorStyle}
                                errorMessage=''
                                value={mobileNo}
                                name="mobileNo"
                              
                                onChangeText={(mobileNo) => { this.setState({ mobileNo }) }}
                                containerStyle={{ color: 'green' }}
                                leftIcon={
                                    <Icon
                                        name='call'
                                        size={styles.iconSize}
                                        style={styles.iconStyle}
                                        color='black'
                                    />
                                }
                            />
                        </View>
                        {/* <View>
                            <Button
                                title={Label_Input_Data.AstroLoginData.updateProfileButton}
                                loading={false}
                                loadingProps={styles.loadingPropsStyle}
                                buttonStyle={[styles.buttonStyle, { backgroundColor: 'grey' }]}
                                titleStyle={{ fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: 16, }}
                                containerStyle={styles.buttonContStyle}
                                onPress={() => this.astronUpdateProfile()}
                            />
                        </View> */}
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    loadingPropsStyle: { size: 'small', color: 'white' },
    buttonStyle: {
        backgroundColor: 'rgba(111, 202, 186, 1)',
        borderRadius: 15,
        padding: 12
    },
    imageStyle: { height: screenHeight / 6, width: '100%' },
    iconSize: 24,
    iconStyle: {
        padding: 5,
    },
    errorColorStyle: {
        color: 'red',
    },
    buttonContStyle: {
        marginHorizontal: 12,
        height: 48,
        marginVertical: 5,
    },
    inputLabelStyle: { color: 'grey', fontSize: 16, paddingLeft: 10, fontFamily: Fonts.Roboto.Roboto_Black },
});


export default ProfileScreen;