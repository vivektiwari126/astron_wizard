import React, { Component, useContext, useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, TextInput, Dimensions, ToastAndroid, ScrollView, TouchableOpacity, SafeAreaView, Platform, Linking, ActivityIndicator } from 'react-native';
import LoginComponent from "../../component/LoginComponent";
import { Images } from "../../styles/Images";
import { Fonts } from "../../styles/Fonts";
import { inputValFlag } from "../../services/paytmservice/PaytmService";
import { Label_Input_Data } from "../../label/Label_Input_Data";
import { Constants } from "../../constant/Constants";
import { UserContext } from '../../services/logincontext/UserContext'
const screenHeight = Dimensions.get('window').height
import { Button, ThemeProvider, Divider, Input, Icon, SocialIcon, Image } from 'react-native-elements';
class SettingScreen extends Component {
    static contextType = UserContext
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            userData: (!inputValFlag(Constants.LogIn_User) ? JSON.parse(Constants.LogIn_User) : ""),
        }
    }
    componentDidMount() {
        console.log("SettingScreen----", this.state.userData)
    }

    astronLogOut = () => {
        const { setUserLogin, userLogin } = this.context;
        Constants.LogIn_User = "";
        setUserLogin(null);
      //  Constants.LogIn_User = "";
    }

    render() {
        const { fullName, emailId, mobileNo, password, confirmPassword } = this.state
        return (
            <View style={{ flex: 1 }} >
                <ScrollView style={{ height: screenHeight }}>
                    <View>
                        <View style={{ height: screenHeight / 4, marginTop: screenHeight / 40 }}>
                            <Image source={Images.logo} resizeMode="center" style={styles.imageStyle} />
                        </View>
                        <View>
                            <Button
                                title={Label_Input_Data.AstroLoginData.logOutButton}
                                loading={false}
                                loadingProps={styles.loadingPropsStyle}
                                buttonStyle={[styles.buttonStyle]}
                                titleStyle={{ fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: 16, }}
                                containerStyle={styles.buttonContStyle}
                                onPress={() => this.astronLogOut()}
                            />
                        </View>
                        <TouchableOpacity onPress={() => { this.props.navigation.navigate(Label_Input_Data.BottomNavigateData.Home); }}>
                            <View style={{ marginTop: 10, marginBottom: 5 }}>
                                <Text style={{
                                    textAlign: 'center',
                                    fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: 18, textDecorationStyle: "solid",
                                    textDecorationColor: "#000", textDecorationLine: "underline",
                                }}> Back </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    loadingPropsStyle: { size: 'small', color: 'white' },
    buttonStyle: {
        backgroundColor: '#ffc107',
        borderRadius: 15,
        padding: 12
    },
    imageStyle: { height: screenHeight / 5, width: '100%' },
    buttonContStyle: {
        marginHorizontal: 12,
        height: 48,
        marginVertical: 5,
    },
});


export default SettingScreen;