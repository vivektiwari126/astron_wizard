import React, { Component, useContext, useState, useEffect } from 'react';
import { View, StyleSheet, FlatList, ScrollView, TextInput, Image, ToastAndroid, TouchableOpacity, Platform, Linking, ActivityIndicator } from 'react-native';
import { Text, Card, Button, Icon } from 'react-native-elements';
import { Images } from "../../styles/Images";
import { Fonts } from "../../styles/Fonts";
import { Label_Input_Data, } from "../../label/Label_Input_Data";
import { knowlarityCall, astronGetAstrologerList, astronCheckAstroStatus, astronCustBalance, astronCustomerCallInitiate } from "../../services/paytmservice/PaytmService";
import { UserContext } from "../../services/logincontext/UserContext";
import { inputValFlag } from "../../services/paytmservice/PaytmService";
import { UserBalanceContext } from "../../services/logincontext/UserBalanceContext";
import { Constants } from "../../constant/Constants";

import { withNavigation } from "react-navigation";
class HomeScreen extends Component {
    static contextType = UserContext
    // static contextType = UserBalanceContext
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            k_number: Constants.K_NUMBER,
            agent_number: '',
            customer_number: '',
            caller_id: Constants.CALLER_ID,
            userData: Constants.LogIn_User,
            astrologerList: [],
            // user balance payment
            userBalance: 0,

            // user call_duration
            total_call_duration: 1,
        }
    }


    componentDidMount() {
        this.unsubscribe = this.props.navigation.addListener('focus', () => {
            const { setUserLogin, userLogin } = this.context;
            this.getAstrologerData()
            if (userLogin !== null && userLogin !== "") {
                const mobileNo = userLogin.data.mobileNo;
                console.log("HomeScreen----", mobileNo)
                this.setState({ customer_number: "+91" + mobileNo });
                console.log("HomeScreen----", this.state.customer_number)
            }
        });
    }

    componentWillUnmount() {
        // Remove the event listener
        this.unsubscribe();
        // this.focusListener.remove();
    }

    paytmPayment = async (item) => {
       
        this.astronCustomerBalance();
        this.getAstrologerData();
        if (item.isAvailable != "Available") {
            if (item.isAvailable == "Busy") {
                ToastAndroid.show(Label_Input_Data.MESSAGE.busyAstroMsg, ToastAndroid.LONG);
                return true;
            } else {
                ToastAndroid.show(Label_Input_Data.MESSAGE.notAvailMsg, ToastAndroid.LONG);
                return true;
            }
        }
        if (Constants.LoginUserBalance >= item.astrologerRate) {

            const total_call_duration = Constants.LoginUserBalance / item.astrologerRate;

            var jsonData = {};
            jsonData.astrologerMobileNo = item.astrologerMobileNo;
            const astroCheckStatu = await astronCheckAstroStatus(jsonData);

            if (astroCheckStatu.status == 1) {

                var jsonDataNested = {};
                jsonDataNested.astrologerMobileNo = item.astrologerMobileNo;
                jsonDataNested.callDuration = total_call_duration;
                jsonDataNested.mobileNo = this.state.customer_number;
                const { setUserLogin, userLogin } = this.context;
                jsonDataNested.emailId = userLogin.data.emailId;

                jsonDataNested.astroCallRate = item.astrologerRate;
                const astronCustCallInitiate = await astronCustomerCallInitiate(jsonDataNested);

                if (astronCustCallInitiate.status == 1) {
                    ToastAndroid.show(astronCustCallInitiate.msg, ToastAndroid.LONG);
                    const getToken = await knowlarityCall(this.state.k_number, item.astrologerMobileNo,
                        this.state.customer_number, this.state.caller_id, total_call_duration);// this.state.total_call_duration);
                        Constants.LoginUserBalance=0;
               //     this.getAstrologerData()
                } else {
                    ToastAndroid.show(astronCustCallInitiate.msg, ToastAndroid.LONG);
                }



            } else {
                ToastAndroid.show(astroCheckStatu.msg, ToastAndroid.LONG);
            }

            console.log('====================================');
            console.log(total_call_duration);
            console.log('====================================');

            // const getToken = await knowlarityCall(this.state.k_number, item.astrologerMobileNo,
            //     this.state.customer_number, this.state.caller_id,total_call_duration);// this.state.total_call_duration);
        } else {
            ToastAndroid.show(Label_Input_Data.MESSAGE.rechargeMsg, ToastAndroid.LONG);
            this.props.navigation.navigate(Label_Input_Data.BottomNavigateData.Recharge)

        }
    }

    getAstrologerData = async () => {
        const astrologerDataResp = await astronGetAstrologerList();
        if (astrologerDataResp.status == 1) {
            //  ToastAndroid.show(astrologerDataResp.msg, ToastAndroid.LONG);
            this.setState({ astrologerList: astrologerDataResp.data });

        } else {
            ToastAndroid.show(astrologerDataResp.msg, ToastAndroid.LONG);
        }


    }


    astronCustomerBalance = async () => {
        var jsonData = {};
        const { setUserLogin, userLogin } = this.context;
        jsonData.emailId = userLogin.data.emailId;
        const astronCustomerBalanceResp = await astronCustBalance(jsonData);
        if (astronCustomerBalanceResp.status == 1) {
            var arrayAmount = 0;
            astronCustomerBalanceResp.data.map((item, index) => {
                return arrayAmount = arrayAmount + Number(item.amount)
            })
            Constants.LoginUserBalance = arrayAmount;
        }
    }


    render() {
        const { astrologerList } = this.state
        return (
            <ScrollView>
                <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                    {astrologerList.map((item, index) => (
                        <View key={index} style={{ width: '50%' }}>
                            <Card containerStyle={styles.cardStyle}>
                                <Card.Title
                                    style={styles.cardTitleStyle}
                                >{item.astrologerName}</Card.Title>
                                <Card.Divider />
                                <Card.Image
                                    style={styles.cardImageStyle}
                                    source={{ uri: item.astrologerPic }}
                                />
                                <Text style={styles.astroYearStyle}>{item.expAstrology}</Text>
                                <Text style={[styles.astroTypeStyle, { fontSize: 10, color: 'black' }]}>{item.astroType}</Text>
                                <Text style={[styles.astroTypeStyle, { color: 'green', fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: 12 }]}>{item.astrologerRate} Rs/Min</Text>
                                <View>
                                    <Text style={{
                                        fontFamily: Fonts.Roboto.Roboto_Black, color: item.isAvailable == "Available" ? "green" : (item.isAvailable == "Busy" ? "red" : "grey"),
                                        textAlign: 'center', fontSize: 12, paddingTop: 2
                                    }}>{item.isAvailable == "Available" ? " * Available " : (item.isAvailable == "Busy" ? " * Busy" : "* Offline ")}</Text>
                                </View>
                                <Button
                                    title={item.callButton}
                                    loading={false}
                                    loadingProps={styles.loadingPropsStyle}
                                    buttonStyle={styles.buttonStyle}
                                    titleStyle={{ fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: 12, }}
                                    containerStyle={styles.buttonContStyle}
                                    onPress={() => { this.paytmPayment(item) }}
                                />
                            </Card>
                        </View>
                    ))
                    }

                </View>
            </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    buttonContStyle: {
        marginHorizontal: 12,
        height: 40,
        marginTop: 20,
    },
    cardTitleStyle: { fontFamily: Fonts.Roboto.Roboto_Black, color: 'red', fontSize: 14 },
    buttonStyle: {
        backgroundColor: '#f57b42',
        borderRadius: 15,
        padding: 9
    },
    cardStyle: { borderRadius: 10, padding: 8 },
    astroYearStyle: {
        fontFamily: Fonts.Roboto.Roboto_Bold,
        textAlign: 'center', fontSize: 13, paddingTop: 7
    },
    astroTypeStyle: {
        fontFamily: Fonts.Awesome.FontAwesome5_Brands, color: 'grey',
        textAlign: 'center', fontSize: 12.5, paddingTop: 4
    },
    cardImageStyle: {
        padding: 0, borderRadius: 4
    }
})


export default withNavigation(HomeScreen);