export const Images = {
    facebook: require('../assets/images/facebook.png'),
    logo: require('../assets/images/logo.png'),
    google: require('../assets/images/google.png'),
    mobilemoney: require('../assets/images/mobilemoney.png'),
    home: require('../assets/images/home.png'),
    user: require('../assets/images/user.png'),

    // Home Page Image
    astroloBanner: require('../assets/images/astrolo.jpeg'),
    astro1: require('../assets/images/astro1.jpg'),
    astro2: require('../assets/images/astro2.jpg'),
    astro3: require('../assets/images/astro3.jpg'),
    astro4: require('../assets/images/astro4.jpg'),
    astro5: require('../assets/images/astro5.jpg'),
    astro6: require('../assets/images/astro6.jpg'),
    astro7: require('../assets/images/astro7.jpg'),
    astro8: require('../assets/images/astro8.jpg'),

    //-------------------- Header Icon --------------
    menu: require('../assets/images/menu.png'),
    digitalWallet: require('../assets/images/digitalWallet.png'),
}