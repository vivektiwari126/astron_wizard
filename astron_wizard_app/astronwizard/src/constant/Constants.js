
export const Constants = {
    MID: "JDeEOP82159566398835",   //TESTING  "lgWxQK17616418802528",  // PROD "JDeEOP82159566398835",
    API_URL: "http://astronwizard.guru/astron_backend_api/token.php",
    CALL_BACK_URL: "https://securegw.paytm.in/theia/processTransaction",//"paytmMID",
    URL_SCHEME: "paytmMIDJDeEOP82159566398835",// PROD  "paytmMIDJDeEOP82159566398835",    // Testing "paytmMIDlgWxQK17616418802528",

    //------------knowlarity Api ----------------
    initiateTransactionUrl: "https://securegw.paytm.in/theia/api/v1/initiateTransaction",
    knowlarityApi: "https://kpi.knowlarity.com/Basic/v1/account/call/makecall",
    knowlarityKEY: "1yWupUw5p26qOU7Vc9Un85GoDw09X0mP8c4CYlW5",
    knowlarityAuthToken: "be77968f-5b10-497a-9566-994d862926e5",
    K_NUMBER: "+919513630936",
    CALLER_ID: "+918048159527",


    LogIn_User: "",
    LoginUserBalance: 0,
    //------------------------- Firebase DataseNode Start----------------------
    LOGIN_NODE: "/AstronWizard/Login",
    USERS_NODE: "/AstronWizard/Users",
    SIGNUP_URL: "http://astronwizard.guru/astron_backend_api/signup.php",
    LOGIN_URL: "http://astronwizard.guru/astron_backend_api/login.php",
    PAYMENT_CONFIRM_URL: "http://astronwizard.guru/astron_backend_api/paymentConfirm.php",

    ASTROLOGER_GET_URL: "http://astronwizard.guru/astron_backend_api/astrologer.php",
    CUSTOMER_BALANCE_URL: "http://astronwizard.guru/astron_backend_api/balance.php",
    ASTROLOGER_CHECKSTATUS_URL: "http://astronwizard.guru/astron_backend_api/checkastrostatus.php",
    CUSTOMER_CALL_INITIATE_URL: "http://astronwizard.guru/astron_backend_api/callInitiated.php",
    FREE_RECHARGE_ASTRON_URL: "http://astronwizard.guru/astron_backend_api/user_free_recharge.php",
    ASTRON_RECHARGE_AMOUNT_URL:"http://astronwizard.guru/astron_backend_api/astrorechargeamt.php"
}