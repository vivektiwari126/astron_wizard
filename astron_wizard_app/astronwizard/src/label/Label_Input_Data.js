import { Images } from "../styles/Images";

export const Label_Input_Data = {
  NavigationData: {
    Login: 'Login',
    SignUp: 'SignUp',

  },
  AstroLoginData: {
    emailLabel: "Email-Id",
    emailPlaceHolder: 'Enter your email id',
    passLabel: "Password",
    passPlaceHolder: 'Enter your password',
    fbLabel: "Login With Facebook",
    googleLabel: "Login With Google",
    loginButton: 'Log in',
    signUpButton: 'Register now',
    updateProfileButton: "Update Profile",
    logOutButton: "Log Out",
  },
  SignUpData: {
    nameLabel: "Full Name",
    namePlaceHolder: "Enter your full name",
    emailLabel: "Email-Id",
    emailPlaceHolder: 'Enter your email id',
    mobileNoLabel: "Mobile No.",
    mobileNoPlaceHolder: 'Enter your mobile no.',
    passLabel: "Password",
    passPlaceHolder: 'Enter your password',
    confirmPassLabel: "Confirm Password",
    confirmPassPlaceHolder: 'Enter your confirm password',
  },
  RechargeInput_Data: {
    enterAmountPlaceHolder: "Enter Amount in INR...",
    proceedButton: "Proceed",
    addMoneyWalletLabel: "Add Money to Wallet",
    availableBalanceLabel: "Available Balance    :   "
  },
  BottomNavigateData: {
    Home: "Home",
    Recharge: "Recharge",
    Profile: "Profile",
  },
  RechargeAstronData: [
    {
      id: 10,
      amountRecharge: "₹  5",
      extraAmtPercentage: 0,
      amount: "5",
    },
    {
      id: 11,
      amountRecharge: "₹  10",
      extraAmtPercentage: 0,
      amount: "10",
    },
    {
      id: 12,
      amountRecharge: "₹  15",
      extraAmtPercentage: 10,
      amount: "15",
    },
    {
      id: 13,
      amountRecharge: "₹  20",
      extraAmtPercentage: 0,
      amount: "20",
    },

    {
      id: 14,
      amountRecharge: "₹  100",
      extraAmtPercentage: 0,
      amount: "100",
    },
    {
      id: 15,
      amountRecharge: "₹  150",
      extraAmtPercentage: 10,
      amount: "150",
    },
    {
      id: 16,
      amountRecharge: "₹  200",
      extraAmtPercentage: 0,
      amount: "200",
    },
    {
      id: 17,
      amountRecharge: "₹  300",
      extraAmtPercentage: 0,
      amount: "300",
    },
    {
      id: 18,
      amountRecharge: "₹  400",
      extraAmtPercentage: 0,
      amount: "400",
    },
    {
      id: 19,
      amountRecharge: "₹  500",
      extraAmtPercentage: 0,
      amount: "500",
    },
    {
      id: 20,
      amountRecharge: "₹  600",
      extraAmtPercentage: 0,
      amount: "600",
    },
    {
      id: 21,
      amountRecharge: "₹  700",
      extraAmtPercentage: 0,
      amount: "700",
    },
    {
      id: 22,
      amountRecharge: "₹  800",
      extraAmtPercentage: 0,
      amount: "800",
    },
    {
      id: 23,
      amountRecharge: "₹  900",
      extraAmtPercentage: 0,
      amount: "900",
    }

  ],


  // JSON Input Field


  Json_Input_Field: {
    emailId: "emailId",
    password: "password",
    fullName: "fullName",
    mobileNo: "mobileNo",
    confirmPassword: "confirmPassword"

  },

  MESSAGE: {
    emailErrMsg: "Please enter a valid email Id.",
    mobileNoErrMsg: "Please enter mobileno.",
    passErrMsg: "Please enter valid password.",
    fullNameErrMsg: "Please enter fullname.",
    confirmPassErrMsg: "Please enter confirm password.",

    //----Login Successfully MSG----------------
    loginSuccessMsg: "Login Successfully",
    loginErrMsg: "Please enter correct email address.",

    // Call available or not check
    notAvailMsg: "Astrologer not available. Please another astrologer.",
    busyAstroMsg: "Astrologer is busy. Please another astrologer.",
    // 
    rechargeMsg:"Please recharge you have not sufficient balance."
  }

}