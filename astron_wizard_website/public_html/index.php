<?php include("header.php"); ?>
		
        <section class="as_banner_wrapper">
            <div class="container-fluid">
                <div class="row as_verticle_center">
                    <div class="col-lg-6 col-md-6">
                        <div class="as_banner_slider">
                            <div class="as_banner_detail">
                            <img src="assets/images/astro0.jpg" alt="" class="#"> 
                    	
					        </div>
					              </div>
                                         </div>
					
                 
					<!----<video width="320" height="240" controls>
                      <source src="assets/images/astrolo.mp4" type="video/mp4">
                        <source src="movie.ogg" type="video/ogg">
                      Your browser does not support the video tag.
                                  </video>!---->
					
					
					<!---<div class="as_banner_slider">
                            <div class="as_banner_detail">
                            <img src="assets/images/astrologers.jpg" alt="" class="img-responsive as_hand_bg">  
                        </div>!--->
						
						
					<div class="col-lg-6 col-md-6">
                            <div class="as_banner_slider">
                            <div class="as_banner_detail">
                            <img src="assets/images/astro.jpg" alt="">
                            </div>
                            </div>
				    </div>
					<a href="astrologer.php" class="as_btn" style="margin-top: 10px;margin-left: 940px;">read more</a>
                </div>
            </div>
        </section>
		
        
        
		
        <section class="as_zodiac_sign_wrapper as_padderTop80 as_padderBottom80">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h1 class="as_heading as_heading_center">choose zodiac sign</h1>
                        <p class="as_font14 as_margin0">Get help right now with career, job & interview questions<br> with answers and guidance from expert advice volunteers.</p>


                        <div class="as_zodiac_inner text-left">
                            <div class="row as_verticle_center">
                                <div class="col-lg-3 col-md-12 col-sm-12 col-12">
                                    <ul class="as_sign_ul">
                                        <li class="as_sign_box">
                                            <a href="#">
                                                <span class="as_sign">
                                                <img src="assets/images/svg/sign1.svg" alt="">
                                                </span>
                                                <div>
                                                    <h5>Aries</h5>
                                                    <p>Mar 21 - Apr 19</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="as_sign_box">
                                            <a href="#">
                                                <span class="as_sign">
                                                <img src="assets/images/svg/sign2.svg" alt="">
                                                </span>
                                                <div>
                                                    <h5>Taurus </h5>
                                                    <p>Mar 21 - Apr 19</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="as_sign_box">
                                            <a href="#">
                                                <span class="as_sign">
                                                <img src="assets/images/svg/sign3.svg" alt="">
                                                </span>
                                                <div>
                                                    <h5>Gemini</h5>
                                                    <p>Mar 21 - Apr 19</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="as_sign_box">
                                            <a href="#">
                                                <span class="as_sign">
                                                <img src="assets/images/svg/sign4.svg" alt="">
                                                </span>
                                                <div>
                                                    <h5>Cancer</h5>
                                                    <p>Mar 21 - Apr 19</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="as_sign_box">
                                            <a href="#">
                                                <span class="as_sign">
                                                <img src="assets/images/svg/sign5.svg" alt="">
                                                </span>
                                                <div>
                                                    <h5>Leo</h5>
                                                    <p>Mar 21 - Apr 19</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="as_sign_box">
                                            <a href="#">
                                                <span class="as_sign">
                                                <img src="assets/images/svg/sign6.svg" alt="">
                                                </span>
                                                <div>
                                                    <h5>Virgo</h5>
                                                    <p>Mar 21 - Apr 19</p>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                                    <div class="as_sign_img text-center">
                                        <img src="assets/images/zodiac.png" alt="" class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-12 col-sm-12 col-12">
                                    <ul class="as_sign_ul as_sign_ul_right">
                                        <li class="as_sign_box">
                                            <a href="#">
                                                <span class="as_sign">
                                                <img src="assets/images/svg/sign7.svg" alt="">
                                                </span>
                                                <div>
                                                    <h5>Libra</h5>
                                                    <p>Mar 21 - Apr 19</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="as_sign_box">
                                            <a href="zodiac_single.html">
                                                <span class="as_sign">
                                                <img src="assets/images/svg/sign8.svg" alt="">
                                                </span>
                                                <div>
                                                    <h5>Scorpio</h5>
                                                    <p>Mar 21 - Apr 19</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="as_sign_box">
                                            <a href="#">
                                                <span class="as_sign">
                                                <img src="assets/images/svg/sign9.svg" alt="">
                                                </span>
                                                <div>
                                                    <h5>Sagittarius</h5>
                                                    <p>Mar 21 - Apr 19</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="as_sign_box">
                                            <a href="#">
                                                <span class="as_sign">
                                                <img src="assets/images/svg/sign10.svg" alt="">
                                                </span>
                                                <div>
                                                    <h5>Capricorn</h5>
                                                    <p>Mar 21 - Apr 19</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="as_sign_box">
                                            <a href="#">
                                                <span class="as_sign">
                                                <img src="assets/images/svg/sign11.svg" alt="">
                                                </span>
                                                <div>
                                                    <h5>Capricorn</h5>
                                                    <p>Mar 21 - Apr 19</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="as_sign_box">
                                            <a href="#">
                                                <span class="as_sign">
                                                <img src="assets/images/svg/sign12.svg" alt="">
                                                </span>
                                                <div>
                                                    <h5>Pisces</h5>
                                                    <p>Mar 21 - Apr 19</p>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		
        
       
        <section class="as_whychoose_wrapper as_padderTop80 as_padderBottom50">
            <div class="container">
                <div class="row as_verticle_center">
                    <div class="col-lg-3 col-md-12">
                        <h1 class="as_heading">Why Choose Us</h1>
                        <p class="as_font14 as_margin0">Consectetur adipiscing elit, sed do eiusmod tempor incididuesdeentiut.</p>
                    </div>
                    <div class="col-lg-9 col-md-12">
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="as_whychoose_box text-center">
                                    <span class="as_number"><span><span data-from="0" data-to="512"
                                        data-speed="5000">512</span>+</span><img src="assets/images/svg/shape.svg" alt=""></span>
                                    <h4>Qualified Astrologers</h4>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="as_whychoose_box text-center">
                                    <span class="as_number"><span><span data-from="0" data-to="62"
                                        data-speed="5000">62</span>+</span><img src="assets/images/svg/shape.svg" alt=""></span>
                                    <h4>Success Horoscope</h4>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="as_whychoose_box text-center">
                                    <span class="as_number"><span><span data-from="0" data-to="94"
                                        data-speed="5000">94</span>+</span><img src="assets/images/svg/shape.svg" alt=""></span>
                                    <h4>Offices Worldwide</h4>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="as_whychoose_box text-center">
                                    <span class="as_number"><span><span data-from="0" data-to="452"
                                        data-speed="5000">452+</span>+</span><img src="assets/images/svg/shape.svg" alt=""></span>
                                    <h4>Trust by million clients</h4>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="as_whychoose_box text-center">
                                    <span class="as_number"><span><span data-from="0" data-to="12"
                                        data-speed="5000">12</span>+</span><img src="assets/images/svg/shape.svg" alt=""></span>
                                    <h4>Year experience</h4>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="as_whychoose_box text-center">
                                    <span class="as_number"><span><span data-from="0" data-to="652"
                                        data-speed="5000">652+</span>+</span><img src="assets/images/svg/shape.svg" alt=""></span>
                                    <h4>Type of horoscopes</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <?php include ("footer.php");?>
        
      
        