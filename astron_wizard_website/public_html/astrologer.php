<?php include ("header.php");?>
		
        <section class="as_breadcrum_wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h1>Astrologer</h1> 

                        <ul class="breadcrumb"> 
                            <li><a href="index.php">Home / Astrologer</a></li>
                            <!--<li>Astrologer</li>-->
                        </ul>
                    </div>
                </div>
            </div>
        </section>
       
        <section class="as_team_wrapper as_padderTop80 as_padderBottom50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                        <div class="as_team_box text-center">
                            <div class="as_team_img">
                                <img src="assets/images/astro1.jpg" alt="" class="img-responsive">
                            </div>
                            <h3 class="as_subheading" style="font-size:18px">Acharya Chandrabhusn </h3>
                            <p style="font-size:12.5px">Experience in Year - 48 </br>
                            <span style="font-size:11px"> Astrologer </span>
                            </p>
                             
                            <div class="as_share_box">
                                <ul>
                                    <li><span style="color:#ffff;background-color: #f57b42;
                                     height: 36px;width:80px;
                                      border-radius: 12%;
                                    font-family: verdana;font-size: 75%;padding:5px;">Call Now</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                        <div class="as_team_box text-center">
                            <div class="as_team_img">
                                <img src="assets/images/astro1a.jpg" alt="" class="img-responsive">
                            </div>
                             <h3 class="as_subheading" style="font-size:18px">Acharya Surendra</h3>
                          <p style="font-size:12.5px">Experience in Year - 32</br>
                            <span style="font-size:12px"> Astrologer </span>
                            </p>
                            <div class="as_share_box">
                                <ul>
                                    <li><span style="color:#ffff;background-color: #f57b42;
                                     height: 36px;width:80px;
                                      border-radius: 12%;
                                    font-family: verdana;font-size: 75%;padding:5px;">Call Now</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                        <div class="as_team_box text-center">
                            <div class="as_team_img">
                                <img src="assets/images/astro2.jpg" alt="" class="img-responsive">
                            </div>
                            <h3 class="as_subheading" style="font-size:18px">Acharya sobhnath </h3>
                           <p style="font-size:12.5px">Experience in Year - 28</br>
                            <span style="font-size:12px"> Astrologer </span>
                            </p>
                            <div class="as_share_box">
                                <ul>
                                    <li><span style="color:#ffff;background-color: #f57b42;
                                     height: 36px;width:80px;
                                      border-radius: 12%;
                                    font-family: verdana;font-size: 75%;padding:5px;">Call Now</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                        <div class="as_team_box text-center">
                            <div class="as_team_img">
                                <img src="assets/images/astro3.jpg" alt="" class="img-responsive">
                            </div>
                            <h3 class="as_subheading"  style="font-size:18px">Acharya Anand</h3>
                           <p style="font-size:12.5px">Experience in Year - 15</br>
                            <span style="font-size:12px"> Astrologer </span>
                            </p>
                            <div class="as_share_box">
                                <ul>
                                    <li><span style="color:#ffff;background-color: #f57b42;
                                     height: 36px;width:80px;
                                      border-radius: 12%;
                                    font-family: verdana;font-size: 75%;padding:5px;">Call Now</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                        <div class="as_team_box text-center">
                            <div class="as_team_img">
                                <img src="assets/images/astro4.jpg" alt="" class="img-responsive">
                            </div>
                            <h3 class="as_subheading" style="font-size:18px">Acharya barunesh</h3>
                           <p style="font-size:12.5px">Experience in Year - 9</br>
                            <span style="font-size:12px"> Astrologer </span>
                            </p>
                            <div class="as_share_box">
                                <ul>
                                     <li><span style="color:#ffff;background-color: #f57b42;
                                     height: 36px;width:80px;
                                      border-radius: 12%;
                                    font-family: verdana;font-size: 75%;padding:5px;">Call Now</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                        <div class="as_team_box text-center">
                            <div class="as_team_img">
                                <img src="assets/images/astro5.jpg" alt="" class="img-responsive">
                            </div>
                            <h3 class="as_subheading" style="font-size:18px">Acharya navneet</h3>
                              <p style="font-size:12.5px">Experience in Year - 9</br>
                            <span style="font-size:12px"> Astrologer </span>
                            </p>
                            <div class="as_share_box">
                                <ul>
                                    <li><span style="color:#ffff;background-color: #f57b42;
                                     height: 36px;width:80px;
                                      border-radius: 12%;
                                    font-family: verdana;font-size: 75%;padding:5px;">Call Now</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                        <div class="as_team_box text-center">
                            <div class="as_team_img">
                                <img src="assets/images/astro6.jpg" alt="" class="img-responsive">
                            </div>
                            <h3 class="as_subheading" style="font-size:18px">Acharya guru</h3>
                            <p style="font-size:12.5px">Experience in Year - 12</br>
                            <span style="font-size:12px"> Astrologer </span>
                            </p>
                            <div class="as_share_box">
                                <ul>
                                    <li><span style="color:#ffff;background-color: #f57b42;
                                     height: 36px;width:80px;
                                      border-radius: 12%;
                                    font-family: verdana;font-size: 75%;padding:5px;">Call Now</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                        <div class="as_team_box text-center">
                            <div class="as_team_img">
                                <img src="assets/images/astro7.jpg" alt="" class="img-responsive">
                            </div>
                            <h3 class="as_subheading" style="font-size:18px">Acharya sant</h3>
                            <p style="font-size:12.5px">Experience in Year - 18</br>
                            <span style="font-size:12px"> Astrologer </span>
                            </p>
                            <div class="as_share_box">
                                <ul>
                                     <li><span style="color:#ffff;background-color: #f57b42;
                                     height: 36px;width:80px;
                                      border-radius: 12%;
                                    font-family: verdana;font-size: 75%;padding:5px;">Call Now</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="as_whychoose_wrapper as_padderTop80 as_padderBottom50">
            <div class="container">
                <div class="row as_verticle_center">
                    <div class="col-lg-3 col-md-12">
                        <h1 class="as_heading">Why Choose Us</h1>
                        <p class="as_font14 as_margin0">Get help right now with career, job & interview questions with answers and guidance from expert advice volunteers.</p>
                    </div>
                    <div class="col-lg-9 col-md-12">
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="as_whychoose_box text-center">
                                    <span class="as_number"><span><span data-from="0" data-to="512"
                                        data-speed="5000">512</span>+</span><img src="assets/images/svg/shape.svg" alt=""></span>
                                    <h4>Qualified Astrologers</h4>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="as_whychoose_box text-center">
                                    <span class="as_number"><span><span data-from="0" data-to="62"
                                        data-speed="5000">62</span>+</span><img src="assets/images/svg/shape.svg" alt=""></span>
                                    <h4>Success Horoscope</h4>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="as_whychoose_box text-center">
                                    <span class="as_number"><span><span data-from="0" data-to="94"
                                        data-speed="5000">94</span>+</span><img src="assets/images/svg/shape.svg" alt=""></span>
                                    <h4>Offices Worldwide</h4>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="as_whychoose_box text-center">
                                    <span class="as_number"><span><span data-from="0" data-to="452"
                                        data-speed="5000">452+</span>+</span><img src="assets/images/svg/shape.svg" alt=""></span>
                                    <h4>Trust by million clients</h4>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="as_whychoose_box text-center">
                                    <span class="as_number"><span><span data-from="0" data-to="12"
                                        data-speed="5000">12</span>+</span><img src="assets/images/svg/shape.svg" alt=""></span>
                                    <h4>Year experience</h4>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="as_whychoose_box text-center">
                                    <span class="as_number"><span><span data-from="0" data-to="652"
                                        data-speed="5000">652+</span>+</span><img src="assets/images/svg/shape.svg" alt=""></span>
                                    <h4>Type of horoscopes</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

       <?php include ("footer.php");?>