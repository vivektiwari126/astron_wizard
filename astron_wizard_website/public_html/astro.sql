-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 09, 2021 at 07:40 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `astro`
--

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(3) NOT NULL,
  `mobile` varchar(40) NOT NULL,
  `passwrd` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `mobile`, `passwrd`) VALUES
(1, '8888888888', '8888888888'),
(10, '8888888888', 'aaa'),
(11, '8888888888', 'aaa'),
(12, '8888888888', 'aaa'),
(13, '99999', 'aaa'),
(14, '123456789', 'mmm');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(3) NOT NULL,
  `fullname` varchar(25) NOT NULL,
  `mobile` varchar(13) NOT NULL,
  `email` varchar(40) NOT NULL,
  `astrologer` varchar(50) NOT NULL,
  `passwrd` varchar(30) NOT NULL,
  `cpasswrd` varchar(30) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fullname`, `mobile`, `email`, `astrologer`, `passwrd`, `cpasswrd`, `status`) VALUES
(1, 'SHYAMA DEVI', 'roshan@gmail.', '9936658305', 'Rohit Kumar', 'aaa', 'aaa', 1),
(2, 'SHYAMA DEVI', 'roshan@gmail.', '8888888888', 'Rohit Kumar', '', '', 1),
(3, '', '', '', '', '', '', 1),
(4, '', '', '', '', '', '', 1),
(5, '', '', '', '', '', '', 1),
(6, '', '', '', '', '', '', 1),
(7, '', '', '', '', '', '', 1),
(8, 'SHYAMA DEVI', 'roshan@gmail.', '8888888888', 'Rohit Kumar', 'aaa', 'aaa', 1),
(9, 'SHYAMA DEVI', '9555267060', '8888888888', 'Rohit Kumar', 'aaa', 'aaa', 1),
(10, '', '', '8888888888', '', 'aaa', '', 1),
(11, '', '', '8888888888', '', 'aaa', '', 1),
(12, 'SHYAMA DEVI', 'roshan@gmail.', '99999', 'Rohit Kumar', 'aaa', 'aaa', 1),
(13, 'SHYAMA DEVI', 'yash@gmail.co', '123456789', 'Rohit Kumar', 'mmm', 'mmm', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
