<?php
session_start();
$loginuser= $_SESSION['loggedin'];
?>
<section class="as_copyright_wrapper text-center">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        </div>
                    <div class="col-lg-1">
                        
                         <div class="as_share_box">
                                <ul>
                                    <li><a href="https://www.facebook.com/astronwizard.guru/"><img src="assets/images/svg/facebook.svg" alt=""></a></li>
                                    <!--<li><a href="javascript:;"><img src="assets/images/svg/twitter.svg" alt=""></a></li>-->
                                    <!--<li><a href="javascript:;"><img src="assets/images/svg/google.svg" alt=""></a></li>-->
                                </ul>
                            </div>
                        
                       
                    </div>
                     <div class="col-lg-5" >
                        <p style="text-align:center;padding-top:8px">Copyright &copy; 2021 AstronWizard.Guru. All Right Reserved.</p>
                    </div>
                     <div class="col-lg-3">
                        </div>
                </div>
            </div>
        </section> 
    </div>
    

    <!-- Modal -->
    <div id="as_login" class="modal fade" tabindex="-1" aria-labelledby="as_login" role="dialog">
        <div class="modal-dialog">
    
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">&times;</button>
                    <h4 class="modal-title">Login</h4>
                </div>
                <div class="modal-body">
                    <div class="as_login_box active">
                        <form action="login.php" method="post">
                            <div class="form-group">
                                <input type="text" name="mobile" placeholder="Enter mobile number" class="form-control" id="">
                            </div>
                            <div class="form-group">
                                <input type="text" name="password" placeholder="Enter password here" class="form-control" id="">
                            </div>
                            <div class="form-group">
                                <div class="as_login_data">
                                    <label>Remember me
                                         <input type="checkbox" name="as_remember_me" value="">
                                         <span class="checkmark"></span>
                                    </label>
                                    <a href="reset-password.php">Forgot password ?</a>
                                </div>
                            </div>
                            <div class="text-center">
                                  <input type="submit" name="login"  value="Log In" style="border-radius: 15px;" class="as_btn">
                                <!--<input type="submit" value="submit" name="submit"  class="as_btn">-->
                                <!--<a href="javascript:;" class="as_btn">login</a>-->
                            </div>
                        </form>
                        <p class="text-center as_margin0 as_padderTop20">Create An Account ? <a href="javascript:;" class="as_orange as_signup">SignUp</a></p>
                    </div>
                    <div class="as_signup_box">
                        <form action="register.php" method="post">
                            <div class="form-group">
                                <input type="text" name="fullname" placeholder="Enter name" class="form-control" id="fullname" required>
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" placeholder="Enter email" class="form-control" id="email" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="astrologer" placeholder="Enter Astrologer name" class="form-control" id="astrologer" required>
                            </div>
							<div class="form-group">
                                <input type="number" name="mobile" maxlength="16" placeholder="Enter mobile number" class="form-control" id="mobile" required>
                            </div>
							
                            <div class="form-group">
                                <input type="password" name="password" placeholder="Enter password here" class="form-control" id="password" required>
                            </div>
                            <div class="form-group">
                                <input type="password" name="cpassword" placeholder="Confirm password here" class="form-control" id="cpassword" required>
                            </div>
							<div class="form-group">
                                <select name="gender" class="form-control" id="" required>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
									<option value="other">Other</option>
                                </select>
                            </div>
                            <div class="text-center">
                                <input type="submit" name="signup" value="Sign Up" style="border-radius: 15px;" class="as_btn">
                            </div>
                        </form>
                        <p class="text-center as_margin0 as_padderTop20">Have An Account ? <a href="javascript:;" class="as_orange as_login">Login</a></p>
                    </div> 
                </div>
            </div>
    
        </div>
    </div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
     <!-- Modal content User Profile Start-->
     <div id="userprofile" class="modal fade" tabindex="-1" aria-labelledby="as_userprofile" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">&times;</button>
                    <h4 class="modal-title">Customer Profile</h4>
                </div>
                <div class="modal-body">
                    <div class="as_login_box active">
                        <form action="logout.php" method="post">
                            <div class="form-group">
                                <text>Name :    <?php echo  "{$loginuser[2]}"; ?>  </text>
                            </div>
                            <div class="form-group">
                               <text>Mobile No :   <?php echo  "{$loginuser[5]}"; ?> </text>
                            </div>
                            <div class="form-group">
                               <text>Email ID :   <?php echo  "{$loginuser[6]}"; ?> </text>
                            </div>
                            <div class="text-center">
                             <input type="submit" name="logout" value="logout" style="border-radius: 15px;"  class="as_btn">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    
        </div>
    </div>
    
    <!-- Modal content User Profile End-->
    
    
    
     <!-- Modal content User Recharge Start-->
     <div id="recharge" class="modal fade" tabindex="-1" aria-labelledby="as_recharge" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">&times;</button>
                    <h4 class="modal-title">Customer Recharge</h4>
                </div>
                <div class="modal-body" >
                    <div class="as_login_box active" >
                        <form action="logout.php" method="post">
                            <div class="form-group" style="display: flex;flex-direction: row;margin-bottom:20px;">
                                <div style=" margin:20px;">
                                <text style="padding-left:45px;padding-right:45px;
                                padding-top:10px;padding-bottom:10px;
                                background-color:grey;
                                border: 1px solid grey;color:#fff;border-radius:20px"> Rs.100 </text>
                                </div>
                                
                                <div  style=" margin:20px;">
                                <text style="padding-left:45px;padding-right:45px;
                                padding-top:10px;padding-bottom:10px;
                                background-color:grey;
                                border: 1px solid grey;color:#fff;border-radius:20px"> Rs.100 </text>
                                </div>
                            </div>
                            
                              <div class="form-group" style="display: flex;flex-direction: row;margin-bottom:30px;">
                               <div style=" margin:20px;">
                                <text style="padding-left:45px;padding-right:45px;
                                padding-top:10px;padding-bottom:10px;
                                background-color:grey;
                                border: 1px solid grey;color:#fff;border-radius:20px"> Rs.100 </text>
                                </div>
                                <div  style=" margin:20px;">
                                <text style="padding-left:45px;padding-right:45px;
                                padding-top:10px;padding-bottom:10px;
                                background-color:grey;
                                border: 1px solid grey;color:#fff;border-radius:20px"> Rs.100 </text>
                                </div>
                            </div>
                            </div>
                            
                            
                               <!--<text>400:   <?php echo  "{$loginuser[6]}"; ?> </text>-->
                            <div class="text-center">
                             <input type="submit" name="recharge" value="Recharge Now" style="border-radius: 15px;"  class="as_btn">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    
        </div>
    </div>
    
    <!-- Modal content User Recharge End-->
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    <!-- javascript -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/plugin/slick/slick.min.js"></script>
    <script src="assets/js/plugin/countto/jquery.countTo.js"></script>
    <script src="assets/js/plugin/airdatepicker/datepicker.min.js"></script>
    <script src="assets/js/plugin/airdatepicker/i18n/datepicker.en.js"></script>
    <script src="assets/js/custom.js"></script>  
</body>
</html>